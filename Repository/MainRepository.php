<?php

namespace App\Repository;


use App\Provider\Mysql;

class MainRepository
{
    /**
     * @param array $authors
     * @return array
     */
    public function getBooksByAuthors(array $authors)
    {
        $authors = array_unique($authors);
        $in = str_repeat('?,', count($authors) - 1) . '?';
        $sql =
            "
            SELECT
            *
            FROM books
            INNER JOIN (
                SELECT books_authors.book_id AS id FROM books_authors 
                INNER JOIN authors ON books_authors.author_id = authors.id
                WHERE (authors.name IN ($in))
                GROUP BY books_authors.book_id
                HAVING count(*) = ?           
            ) author_filter ON books.id = author_filter.id
            ";

        $params = $authors;
        $params[] = count($authors);

        return $this->getResult($sql, $params);
    }

    /**
     * @param string $books
     * @return array
     */
    public function getAuthorsByBook(string $books)
    {
        $sql =
            "
            SELECT DISTINCT 
            authors.*
            FROM authors
            INNER JOIN books_authors ON books_authors.author_id = authors.id
            INNER JOIN books ON books.id = books_authors.book_id
            WHERE (books.name = ?)             
            ";

        return $this->getResult($sql, [$books]);
    }

    /**
     * @param int $number
     * @return array
     */
    public function getBooksWithNumberAuthors(int $number)
    {
        $sql =
            "
            SELECT 
            books.*,
            count(*) AS num_authors
            FROM books_authors
            INNER JOIN books ON books.id = books_authors.book_id
            GROUP BY books_authors.book_id
            HAVING num_authors = ? 
            ";

        $params = [$number];

        return $this->getResult($sql, $params);
    }

    private function pdo(): \PDO
    {
        return Mysql::getConnection();
    }

    private function getResult($sql, $params)
    {
        $pdo = $this->pdo();
        $statement = $pdo->prepare($sql);
        $statement->execute($params);

        $result = [];
        while ($row = $statement->fetchObject()) {
            $result[] = $row;
        };
        return $result;
    }
}