<?php

namespace App\Provider;

use PDO;

class Mysql
{
    static private $connection;

    private function __construct()
    {
        // Singleton - for short code
    }

    /**
     * @return PDO
     */
    static public function getConnection(): PDO
    {
        if (empty(self::$connection)) {
            self::$connection = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
        }

        return self::$connection;
    }
}