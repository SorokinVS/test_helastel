-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Фев 10 2020 г., 17:37
-- Версия сервера: 8.0.12
-- Версия PHP: 7.2.10

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_helastel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`id`, `name`) VALUES
(3, 'Азимов'),
(9, 'Влиссидес'),
(15, 'Воронин'),
(6, 'Гамма'),
(1, 'Гончаров'),
(14, 'Грачёв'),
(8, 'Джонсон'),
(16, 'Жуков'),
(10, 'Иванов'),
(4, 'Лем'),
(17, 'Муравьёв'),
(11, 'Петров'),
(2, 'Пушкин'),
(12, 'Сидоров'),
(13, 'Сорокин'),
(7, 'Хелм');

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `name`) VALUES
(4, 'Евгений Онегин'),
(15, 'Жизнь как она есть'),
(9, 'Край основания'),
(5, 'Медный всадник'),
(1, 'Обрыв'),
(3, 'Обыкновенная история'),
(7, 'Основание'),
(8, 'Основание и империя'),
(13, 'Приёмы объектно-ориентированного проектирования'),
(14, 'Примечательные фамилии России'),
(16, 'Семейство Врановых'),
(6, 'сказка о рыбаке и рыбке'),
(10, 'Совершенный робот'),
(12, 'Солярис'),
(11, 'Человек с Марса');

-- --------------------------------------------------------

--
-- Структура таблицы `books_authors`
--

CREATE TABLE `books_authors` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books_authors`
--

INSERT INTO `books_authors` (`book_id`, `author_id`) VALUES
(1, 1),
(3, 1),
(4, 2),
(5, 2),
(6, 2),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 4),
(12, 4),
(13, 6),
(13, 7),
(13, 8),
(13, 9),
(14, 10),
(14, 11),
(14, 12),
(15, 10),
(15, 11),
(15, 14),
(16, 13),
(16, 14),
(16, 15);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_authors_name` (`name`);

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_books_name` (`name`);

--
-- Индексы таблицы `books_authors`
--
ALTER TABLE `books_authors`
  ADD UNIQUE KEY `UK_books_authors` (`book_id`,`author_id`),
  ADD KEY `IDX_books_authors_book_id` (`book_id`),
  ADD KEY `IDX_books_authors_author_id` (`author_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `books_authors`
--
ALTER TABLE `books_authors`
  ADD CONSTRAINT `FK_books_authors_authors_id` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_books_authors_books_id` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
