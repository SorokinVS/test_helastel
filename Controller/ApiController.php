<?php

namespace App\Controller;

use App\Repository\MainRepository;

class ApiController
{
    /**
     * @param mixed $authors
     * @return false|string
     */
    public function booksByAuthors($authors)
    {
        if (!is_array($authors)) {
            $authors = [$authors];
        }
        $repository = new MainRepository();
        return json_encode($repository->getBooksByAuthors($authors));
    }

    /**
     * @param string $book
     * @return false|string
     */
    public function authorsByBook(string $book)
    {
        $repository = new MainRepository();
        return json_encode($repository->getAuthorsByBook($book));
    }

    /**
     * @param int $number
     * @return false|string
     */
    public function booksWithNumberAuthors(int $number)
    {
        $repository = new MainRepository();
        return json_encode($repository->getBooksWithNumberAuthors($number));
    }
}