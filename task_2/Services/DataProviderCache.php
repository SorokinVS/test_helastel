<?php


namespace App\task_2\Services;


use App\task_2\Exceptions\WrongDataProviderInput;

class DataProviderCache implements DataProviderInterface
{
    private $cache;
    private $logger;
    private $dataProvider;

    /**
     * DataProviderCache constructor.
     * @param CacheItemPoolInterface $cache
     * @param LoggerInterface $logger
     * @param DataProviderInterface $dataProvider
     */
    function __construct(CacheItemPoolInterface $cache, LoggerInterface $logger, DataProviderInterface $dataProvider)
    {
        $this->cache = $cache;
        $this->logger = $logger;
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param array $input
     * @return array
     */
    public function get(array $input): array
    {
        try {
            $cacheKey = $this->getCacheKey($input);
        } catch (WrongDataProviderInput $e) {
            $this->logger->critical('Error: WrongDataProviderInput');
            return [];
        }

        try {
            $cacheItem = $this->cache->getItem($cacheKey);
            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = $this->dataProvider->get($input);

            $cacheItem->set($result);

            return $result;

        } catch (Exception $e) {
            $this->logger->critical('Unknown error');
        }

        return [];
    }

    /**
     * @param array $input
     * @return string
     * @throws WrongDataProviderInput
     */
    private function getCacheKey(array $input)
    {
        $key = json_encode($input);
        if ($key === false) {
            throw new WrongDataProviderInput();
        }
        return $key;
    }
}