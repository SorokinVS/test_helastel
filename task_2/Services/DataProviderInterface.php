<?php


namespace App\task_2\Services;


interface DataProviderInterface
{
    public function get(array $request):array;
}