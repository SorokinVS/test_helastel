<?php

use App\Controller\ApiController;

const DB_USER = 'root';
const DB_PASSWORD = 'root';
const DB_DSN = 'mysql:host=localhost;dbname=test_helastel';

require_once 'Provider/Mysql.php';
require_once 'Controller/ApiController.php';
require_once 'Repository/MainRepository.php';

$actionName = trim($_SERVER['REDIRECT_URL'], ' /');

$controller = new ApiController();

if (!method_exists($controller, $actionName)) {
    header("HTTP/1.1 404 Not Found");
    exit();
}

if (!isset($_GET['q'])) {
    header("HTTP/1.1 400 Bad Request");
    exit();
}

try {
    $result = $controller->$actionName($_GET['q']);
} catch (Exception $e) {
    header("HTTP/1.1 400 Bad Request");
    exit();
}

header("Content-type: application/json; charset=utf-8");
echo $result;